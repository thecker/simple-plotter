.. image:: https://readthedocs.org/projects/simple-plotter/badge/?version=latest
    :target: https://simple-plotter.readthedocs.io/en/latest/?badge=latest
    :alt: Documentation Status

.. image:: https://gitlab.com/thecker/simple-plotter/badges/master/pipeline.svg
    :target: https://gitlab.com/thecker/simple-plotter/commits/master
    :alt: pipeline status

.. image:: https://gitlab.com/thecker/simple-plotter/badges/master/coverage.svg?job=test
    :target: https://thecker.gitlab.io/simple-plotter/index.html
    :alt: Code coverage report for core modules

simple-plotter
==============

*simple-plotter* is a code-generator to create python code for plotting functional 2D x,y-plots.
The function equation has to be entered in python syntax (allowing the use of numpy statements).

*simple-plotter* provides a command line interface (CLI) tool, which takes simple JSON files as inputs to generate the
code.

Graphical user interface (GUI) front-ends for *simple-plotter* are available as well.

The `simple-plotter-qt`_ package provides with a Qt-based GUI-frontend intended for desktop use.

simple-plotter4a_ provides an alternative kivy-based frontend, created primarily for the Android port, which can be
used on a desktop system as well.

See NOTICE and LICENSE files for details on licensing.

For install and usage instructions please see the `project documentation`_.

.. _project documentation: https://simple-plotter.readthedocs.io/en/latest/
.. _simple-plotter4a: https://gitlab.com/thecker/simple-plotter4a
.. _simple-plotter-qt: https://gitlab.com/thecker/simple-plotter-qt