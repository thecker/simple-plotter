.. _licenses:

Licenses
========

This chapter contains the license information of source code and binary releases of *simple-plotter* and related
components.

.. note::

    simple-plotter 0.4.0 or later is released under the MIT license, while
    earlier versions (up to 0.3.2) were mainly released under the GPLv3+ license
    including some code in the advanced_graph module taken from matplotlib under
    the matplotlib license.
    In simple-plotter version 0.4.0 the Qt-frontend (simple_plotter/gui.py) as
    well as the color map data in simple_plotter/core/advanced_graph.py taken
    from matplotlib have been removed to allow for a simpler and more permissive
    license of the core components.
    The Qt-frontend is still available as a separate project called
    'simple-plotter-qt', which remains released under the GPLv3+.

simple-plotter
--------------

.. code-block:: none

    simple-plotter - plot code generator for 2D functional line graphs
    Copyright (C) 2019-2020 Thies Hecker

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.


simple-plotter-qt
-----------------

.. code-block:: none

    simple-plotter-qt - Qt based front-end for simple_plotter
    Copyright (C) 2020  Thies Hecker

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.


simple-plotter4a
----------------

.. code-block:: none

    simple-plotter4a - kivy based front-end for simple_plotter
    Copyright (C) 2020  Thies Hecker

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.


simple-plotter4a binary releases (Android)
------------------------------------------

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   license_details/attributions_0.1.0.rst
