simple-plotter4a rel. 0.1.0
===========================


This chapter shows the copyright and license notes of *simple-plotter4a* and the dependencies used to build *simple-plotter4a*.


.. code-block:: none

    simple-plotter4a - kivy based front-end for simple_plotter
    Copyright (C) 2020  Thies Hecker
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.


Dependencies
------------


python3 3.7.1
~~~~~~~~~~~~~

Copyright (c) 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018 Python Software Foundation; All Rights Reserved

https://www.python.org/



.. code-block:: none

    A. HISTORY OF THE SOFTWARE
    ==========================
    
    Python was created in the early 1990s by Guido van Rossum at Stichting
    Mathematisch Centrum (CWI, see http://www.cwi.nl) in the Netherlands
    as a successor of a language called ABC.  Guido remains Python's
    principal author, although it includes many contributions from others.
    
    In 1995, Guido continued his work on Python at the Corporation for
    National Research Initiatives (CNRI, see http://www.cnri.reston.va.us)
    in Reston, Virginia where he released several versions of the
    software.
    
    In May 2000, Guido and the Python core development team moved to
    BeOpen.com to form the BeOpen PythonLabs team.  In October of the same
    year, the PythonLabs team moved to Digital Creations, which became
    Zope Corporation.  In 2001, the Python Software Foundation (PSF, see
    https://www.python.org/psf/) was formed, a non-profit organization
    created specifically to own Python-related Intellectual Property.
    Zope Corporation was a sponsoring member of the PSF.
    
    All Python releases are Open Source (see http://www.opensource.org for
    the Open Source Definition).  Historically, most, but not all, Python
    releases have also been GPL-compatible; the table below summarizes
    the various releases.
    
        Release         Derived     Year        Owner       GPL-
                        from                                compatible? (1)
    
        0.9.0 thru 1.2              1991-1995   CWI         yes
        1.3 thru 1.5.2  1.2         1995-1999   CNRI        yes
        1.6             1.5.2       2000        CNRI        no
        2.0             1.6         2000        BeOpen.com  no
        1.6.1           1.6         2001        CNRI        yes (2)
        2.1             2.0+1.6.1   2001        PSF         no
        2.0.1           2.0+1.6.1   2001        PSF         yes
        2.1.1           2.1+2.0.1   2001        PSF         yes
        2.1.2           2.1.1       2002        PSF         yes
        2.1.3           2.1.2       2002        PSF         yes
        2.2 and above   2.1.1       2001-now    PSF         yes
    
    Footnotes:
    
    (1) GPL-compatible doesn't mean that we're distributing Python under
        the GPL.  All Python licenses, unlike the GPL, let you distribute
        a modified version without making your changes open source.  The
        GPL-compatible licenses make it possible to combine Python with
        other software that is released under the GPL; the others don't.
    
    (2) According to Richard Stallman, 1.6.1 is not GPL-compatible,
        because its license has a choice of law clause.  According to
        CNRI, however, Stallman's lawyer has told CNRI's lawyer that 1.6.1
        is "not incompatible" with the GPL.
    
    Thanks to the many outside volunteers who have worked under Guido's
    direction to make these releases possible.
    
    
    B. TERMS AND CONDITIONS FOR ACCESSING OR OTHERWISE USING PYTHON
    ===============================================================
    
    PYTHON SOFTWARE FOUNDATION LICENSE VERSION 2
    --------------------------------------------
    
    1. This LICENSE AGREEMENT is between the Python Software Foundation
    ("PSF"), and the Individual or Organization ("Licensee") accessing and
    otherwise using this software ("Python") in source or binary form and
    its associated documentation.
    
    2. Subject to the terms and conditions of this License Agreement, PSF hereby
    grants Licensee a nonexclusive, royalty-free, world-wide license to reproduce,
    analyze, test, perform and/or display publicly, prepare derivative works,
    distribute, and otherwise use Python alone or in any derivative version,
    provided, however, that PSF's License Agreement and PSF's notice of copyright,
    i.e., "Copyright (c) 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010,
    2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018 Python Software Foundation; All
    Rights Reserved" are retained in Python alone or in any derivative version
    prepared by Licensee.
    
    3. In the event Licensee prepares a derivative work that is based on
    or incorporates Python or any part thereof, and wants to make
    the derivative work available to others as provided herein, then
    Licensee hereby agrees to include in any such work a brief summary of
    the changes made to Python.
    
    4. PSF is making Python available to Licensee on an "AS IS"
    basis.  PSF MAKES NO REPRESENTATIONS OR WARRANTIES, EXPRESS OR
    IMPLIED.  BY WAY OF EXAMPLE, BUT NOT LIMITATION, PSF MAKES NO AND
    DISCLAIMS ANY REPRESENTATION OR WARRANTY OF MERCHANTABILITY OR FITNESS
    FOR ANY PARTICULAR PURPOSE OR THAT THE USE OF PYTHON WILL NOT
    INFRINGE ANY THIRD PARTY RIGHTS.
    
    5. PSF SHALL NOT BE LIABLE TO LICENSEE OR ANY OTHER USERS OF PYTHON
    FOR ANY INCIDENTAL, SPECIAL, OR CONSEQUENTIAL DAMAGES OR LOSS AS
    A RESULT OF MODIFYING, DISTRIBUTING, OR OTHERWISE USING PYTHON,
    OR ANY DERIVATIVE THEREOF, EVEN IF ADVISED OF THE POSSIBILITY THEREOF.
    
    6. This License Agreement will automatically terminate upon a material
    breach of its terms and conditions.
    
    7. Nothing in this License Agreement shall be deemed to create any
    relationship of agency, partnership, or joint venture between PSF and
    Licensee.  This License Agreement does not grant permission to use PSF
    trademarks or trade name in a trademark sense to endorse or promote
    products or services of Licensee, or any third party.
    
    8. By copying, installing or otherwise using Python, Licensee
    agrees to be bound by the terms and conditions of this License
    Agreement.
    
    
    BEOPEN.COM LICENSE AGREEMENT FOR PYTHON 2.0
    -------------------------------------------
    
    BEOPEN PYTHON OPEN SOURCE LICENSE AGREEMENT VERSION 1
    
    1. This LICENSE AGREEMENT is between BeOpen.com ("BeOpen"), having an
    office at 160 Saratoga Avenue, Santa Clara, CA 95051, and the
    Individual or Organization ("Licensee") accessing and otherwise using
    this software in source or binary form and its associated
    documentation ("the Software").
    
    2. Subject to the terms and conditions of this BeOpen Python License
    Agreement, BeOpen hereby grants Licensee a non-exclusive,
    royalty-free, world-wide license to reproduce, analyze, test, perform
    and/or display publicly, prepare derivative works, distribute, and
    otherwise use the Software alone or in any derivative version,
    provided, however, that the BeOpen Python License is retained in the
    Software, alone or in any derivative version prepared by Licensee.
    
    3. BeOpen is making the Software available to Licensee on an "AS IS"
    basis.  BEOPEN MAKES NO REPRESENTATIONS OR WARRANTIES, EXPRESS OR
    IMPLIED.  BY WAY OF EXAMPLE, BUT NOT LIMITATION, BEOPEN MAKES NO AND
    DISCLAIMS ANY REPRESENTATION OR WARRANTY OF MERCHANTABILITY OR FITNESS
    FOR ANY PARTICULAR PURPOSE OR THAT THE USE OF THE SOFTWARE WILL NOT
    INFRINGE ANY THIRD PARTY RIGHTS.
    
    4. BEOPEN SHALL NOT BE LIABLE TO LICENSEE OR ANY OTHER USERS OF THE
    SOFTWARE FOR ANY INCIDENTAL, SPECIAL, OR CONSEQUENTIAL DAMAGES OR LOSS
    AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THE SOFTWARE, OR ANY
    DERIVATIVE THEREOF, EVEN IF ADVISED OF THE POSSIBILITY THEREOF.
    
    5. This License Agreement will automatically terminate upon a material
    breach of its terms and conditions.
    
    6. This License Agreement shall be governed by and interpreted in all
    respects by the law of the State of California, excluding conflict of
    law provisions.  Nothing in this License Agreement shall be deemed to
    create any relationship of agency, partnership, or joint venture
    between BeOpen and Licensee.  This License Agreement does not grant
    permission to use BeOpen trademarks or trade names in a trademark
    sense to endorse or promote products or services of Licensee, or any
    third party.  As an exception, the "BeOpen Python" logos available at
    http://www.pythonlabs.com/logos.html may be used according to the
    permissions granted on that web page.
    
    7. By copying, installing or otherwise using the software, Licensee
    agrees to be bound by the terms and conditions of this License
    Agreement.
    
    
    CNRI LICENSE AGREEMENT FOR PYTHON 1.6.1
    ---------------------------------------
    
    1. This LICENSE AGREEMENT is between the Corporation for National
    Research Initiatives, having an office at 1895 Preston White Drive,
    Reston, VA 20191 ("CNRI"), and the Individual or Organization
    ("Licensee") accessing and otherwise using Python 1.6.1 software in
    source or binary form and its associated documentation.
    
    2. Subject to the terms and conditions of this License Agreement, CNRI
    hereby grants Licensee a nonexclusive, royalty-free, world-wide
    license to reproduce, analyze, test, perform and/or display publicly,
    prepare derivative works, distribute, and otherwise use Python 1.6.1
    alone or in any derivative version, provided, however, that CNRI's
    License Agreement and CNRI's notice of copyright, i.e., "Copyright (c)
    1995-2001 Corporation for National Research Initiatives; All Rights
    Reserved" are retained in Python 1.6.1 alone or in any derivative
    version prepared by Licensee.  Alternately, in lieu of CNRI's License
    Agreement, Licensee may substitute the following text (omitting the
    quotes): "Python 1.6.1 is made available subject to the terms and
    conditions in CNRI's License Agreement.  This Agreement together with
    Python 1.6.1 may be located on the Internet using the following
    unique, persistent identifier (known as a handle): 1895.22/1013.  This
    Agreement may also be obtained from a proxy server on the Internet
    using the following URL: http://hdl.handle.net/1895.22/1013".
    
    3. In the event Licensee prepares a derivative work that is based on
    or incorporates Python 1.6.1 or any part thereof, and wants to make
    the derivative work available to others as provided herein, then
    Licensee hereby agrees to include in any such work a brief summary of
    the changes made to Python 1.6.1.
    
    4. CNRI is making Python 1.6.1 available to Licensee on an "AS IS"
    basis.  CNRI MAKES NO REPRESENTATIONS OR WARRANTIES, EXPRESS OR
    IMPLIED.  BY WAY OF EXAMPLE, BUT NOT LIMITATION, CNRI MAKES NO AND
    DISCLAIMS ANY REPRESENTATION OR WARRANTY OF MERCHANTABILITY OR FITNESS
    FOR ANY PARTICULAR PURPOSE OR THAT THE USE OF PYTHON 1.6.1 WILL NOT
    INFRINGE ANY THIRD PARTY RIGHTS.
    
    5. CNRI SHALL NOT BE LIABLE TO LICENSEE OR ANY OTHER USERS OF PYTHON
    1.6.1 FOR ANY INCIDENTAL, SPECIAL, OR CONSEQUENTIAL DAMAGES OR LOSS AS
    A RESULT OF MODIFYING, DISTRIBUTING, OR OTHERWISE USING PYTHON 1.6.1,
    OR ANY DERIVATIVE THEREOF, EVEN IF ADVISED OF THE POSSIBILITY THEREOF.
    
    6. This License Agreement will automatically terminate upon a material
    breach of its terms and conditions.
    
    7. This License Agreement shall be governed by the federal
    intellectual property law of the United States, including without
    limitation the federal copyright law, and, to the extent such
    U.S. federal law does not apply, by the law of the Commonwealth of
    Virginia, excluding Virginia's conflict of law provisions.
    Notwithstanding the foregoing, with regard to derivative works based
    on Python 1.6.1 that incorporate non-separable material that was
    previously distributed under the GNU General Public License (GPL), the
    law of the Commonwealth of Virginia shall govern this License
    Agreement only as to issues arising under or with respect to
    Paragraphs 4, 5, and 7 of this License Agreement.  Nothing in this
    License Agreement shall be deemed to create any relationship of
    agency, partnership, or joint venture between CNRI and Licensee.  This
    License Agreement does not grant permission to use CNRI trademarks or
    trade name in a trademark sense to endorse or promote products or
    services of Licensee, or any third party.
    
    8. By clicking on the "ACCEPT" button where indicated, or by copying,
    installing or otherwise using Python 1.6.1, Licensee agrees to be
    bound by the terms and conditions of this License Agreement.
    
            ACCEPT
    
    
    CWI LICENSE AGREEMENT FOR PYTHON 0.9.0 THROUGH 1.2
    --------------------------------------------------
    
    Copyright (c) 1991 - 1995, Stichting Mathematisch Centrum Amsterdam,
    The Netherlands.  All rights reserved.
    
    Permission to use, copy, modify, and distribute this software and its
    documentation for any purpose and without fee is hereby granted,
    provided that the above copyright notice appear in all copies and that
    both that copyright notice and this permission notice appear in
    supporting documentation, and that the name of Stichting Mathematisch
    Centrum or CWI not be used in advertising or publicity pertaining to
    distribution of the software without specific, written prior
    permission.
    
    STICHTING MATHEMATISCH CENTRUM DISCLAIMS ALL WARRANTIES WITH REGARD TO
    THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
    FITNESS, IN NO EVENT SHALL STICHTING MATHEMATISCH CENTRUM BE LIABLE
    FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
    WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
    ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
    OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


kivy 1.11.1
~~~~~~~~~~~

Copyright (c) 2010-2019 Kivy Team and other contributors

https://kivy.org



.. code-block:: none

    Copyright (c) 2010-2019 Kivy Team and other contributors
    
    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:
    
    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.
    
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.


numpy 1.16.4
~~~~~~~~~~~~

Copyright © 2005-2019, NumPy Developers. All rights reserved.

https://numpy.org/



.. code-block:: none

    Copyright (c) 2005-2019, NumPy Developers.
    All rights reserved.
    
    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are
    met:
    
        * Redistributions of source code must retain the above copyright
           notice, this list of conditions and the following disclaimer.
    
        * Redistributions in binary form must reproduce the above
           copyright notice, this list of conditions and the following
           disclaimer in the documentation and/or other materials provided
           with the distribution.
    
        * Neither the name of the NumPy Developers nor the names of any
           contributors may be used to endorse or promote products derived
           from this software without specific prior written permission.
    
    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
    A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
    OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
    LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
    
    
    
    The NumPy repository and source distributions bundle several libraries that are
    compatibly licensed.  We list these here.
    
    Name: Numpydoc
    Files: doc/sphinxext/numpydoc/*
    License: 2-clause BSD
      For details, see doc/sphinxext/LICENSE.txt
    
    Name: scipy-sphinx-theme
    Files: doc/scipy-sphinx-theme/*
    License: 3-clause BSD, PSF and Apache 2.0
      For details, see doc/scipy-sphinx-theme/LICENSE.txt
    
    Name: lapack-lite
    Files: numpy/linalg/lapack_lite/*
    License: 3-clause BSD
      For details, see numpy/linalg/lapack_lite/LICENSE.txt
    
    Name: tempita
    Files: tools/npy_tempita/*
    License: BSD derived
      For details, see tools/npy_tempita/license.txt
    
    Name: dragon4
    Files: numpy/core/src/multiarray/dragon4.c
    License: One of a kind
      For license text, see numpy/core/src/multiarray/dragon4.c


versioneer 0.18
~~~~~~~~~~~~~~~

https://github.com/warner/python-versioneer



.. code-block:: none

    taken from https://github.com/warner/python-versioneer/blob/0.18/README.md
    
    License
    
    To make Versioneer easier to embed, all its code is dedicated to the public
    domain. The _version.py that it creates is also in the public domain.
    Specifically, both are released under the Creative Commons "Public Domain
    Dedication" license (CC0-1.0), as described in
    https://creativecommons.org/publicdomain/zero/1.0/ .


jinja2 2.11.1
~~~~~~~~~~~~~

Copyright 2007 Pallets

https://jinja.palletsprojects.com



.. code-block:: none

    Copyright 2007 Pallets
    
    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are
    met:
    
    1.  Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
    
    2.  Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
    
    3.  Neither the name of the copyright holder nor the names of its
        contributors may be used to endorse or promote products derived from
        this software without specific prior written permission.
    
    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
    PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
    HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
    TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
    PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
    LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
    NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.



markupsafe 1.1.1
~~~~~~~~~~~~~~~~

Copyright 2010 Pallets

https://palletsprojects.com/p/markupsafe/



.. code-block:: none

    Copyright 2010 Pallets
    
    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are
    met:
    
    1.  Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
    
    2.  Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
    
    3.  Neither the name of the copyright holder nor the names of its
        contributors may be used to endorse or promote products derived from
        this software without specific prior written permission.
    
    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
    PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
    HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
    TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
    PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
    LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
    NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.



setuptools_scm 3.5.0
~~~~~~~~~~~~~~~~~~~~

Ronny Pfannschmidt

https://github.com/pypa/setuptools_scm/



.. code-block:: none

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:
    
    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.
    
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.



kivy-garden.graph 0.4.0
~~~~~~~~~~~~~~~~~~~~~~~

Copyright (c) 2019- Kivy Team and other contributors

https://github.com/kivy-garden/graph



.. code-block:: none

    Copyright (c) 2019- Kivy Team and other contributors
    
    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:
    
    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.
    
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.



matplotlib 3.1.3
~~~~~~~~~~~~~~~~

Copyright (c) 2012-2020 Matplotlib Development Team; All Rights Reserved

https://matplotlib.org/



.. code-block:: none

    License agreement for matplotlib versions 1.3.0 and later
    =========================================================
    
    1. This LICENSE AGREEMENT is between the Matplotlib Development Team
    ("MDT"), and the Individual or Organization ("Licensee") accessing and
    otherwise using matplotlib software in source or binary form and its
    associated documentation.
    
    2. Subject to the terms and conditions of this License Agreement, MDT
    hereby grants Licensee a nonexclusive, royalty-free, world-wide license
    to reproduce, analyze, test, perform and/or display publicly, prepare
    derivative works, distribute, and otherwise use matplotlib
    alone or in any derivative version, provided, however, that MDT's
    License Agreement and MDT's notice of copyright, i.e., "Copyright (c)
    2012- Matplotlib Development Team; All Rights Reserved" are retained in
    matplotlib  alone or in any derivative version prepared by
    Licensee.
    
    3. In the event Licensee prepares a derivative work that is based on or
    incorporates matplotlib or any part thereof, and wants to
    make the derivative work available to others as provided herein, then
    Licensee hereby agrees to include in any such work a brief summary of
    the changes made to matplotlib .
    
    4. MDT is making matplotlib available to Licensee on an "AS
    IS" basis.  MDT MAKES NO REPRESENTATIONS OR WARRANTIES, EXPRESS OR
    IMPLIED.  BY WAY OF EXAMPLE, BUT NOT LIMITATION, MDT MAKES NO AND
    DISCLAIMS ANY REPRESENTATION OR WARRANTY OF MERCHANTABILITY OR FITNESS
    FOR ANY PARTICULAR PURPOSE OR THAT THE USE OF MATPLOTLIB
    WILL NOT INFRINGE ANY THIRD PARTY RIGHTS.
    
    5. MDT SHALL NOT BE LIABLE TO LICENSEE OR ANY OTHER USERS OF MATPLOTLIB
     FOR ANY INCIDENTAL, SPECIAL, OR CONSEQUENTIAL DAMAGES OR
    LOSS AS A RESULT OF MODIFYING, DISTRIBUTING, OR OTHERWISE USING
    MATPLOTLIB , OR ANY DERIVATIVE THEREOF, EVEN IF ADVISED OF
    THE POSSIBILITY THEREOF.
    
    6. This License Agreement will automatically terminate upon a material
    breach of its terms and conditions.
    
    7. Nothing in this License Agreement shall be deemed to create any
    relationship of agency, partnership, or joint venture between MDT and
    Licensee.  This License Agreement does not grant permission to use MDT
    trademarks or trade name in a trademark sense to endorse or promote
    products or services of Licensee, or any third party.
    
    8. By copying, installing or otherwise using matplotlib ,
    Licensee agrees to be bound by the terms and conditions of this License
    Agreement.


libffi 3.3-rc0
~~~~~~~~~~~~~~

Copyright (c) 1996-2014  Anthony Green, Red Hat, Inc and others.

https://sourceware.org/libffi/



.. code-block:: none

    libffi - Copyright (c) 1996-2014  Anthony Green, Red Hat, Inc and others.
    See source files for details.
    
    Permission is hereby granted, free of charge, to any person obtaining
    a copy of this software and associated documentation files (the
    ``Software''), to deal in the Software without restriction, including
    without limitation the rights to use, copy, modify, merge, publish,
    distribute, sublicense, and/or sell copies of the Software, and to
    permit persons to whom the Software is furnished to do so, subject to
    the following conditions:
    
    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.
    
    THE SOFTWARE IS PROVIDED ``AS IS'', WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
    IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
    CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
    TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
    SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.



OpenSSL 1.1.1
~~~~~~~~~~~~~

Copyright (c) 1998-2018 The OpenSSL Project.  All rights reserved. / Copyright (C) 1995-1998 Eric Young (eay@cryptsoft.com), All rights reserved.

http://www.openssl.org/



.. code-block:: none

    
      LICENSE ISSUES
      ==============
    
      The OpenSSL toolkit stays under a double license, i.e. both the conditions of
      the OpenSSL License and the original SSLeay license apply to the toolkit.
      See below for the actual license texts.
    
      OpenSSL License
      ---------------
    
    /* ====================================================================
     * Copyright (c) 1998-2018 The OpenSSL Project.  All rights reserved.
     *
     * Redistribution and use in source and binary forms, with or without
     * modification, are permitted provided that the following conditions
     * are met:
     *
     * 1. Redistributions of source code must retain the above copyright
     *    notice, this list of conditions and the following disclaimer.
     *
     * 2. Redistributions in binary form must reproduce the above copyright
     *    notice, this list of conditions and the following disclaimer in
     *    the documentation and/or other materials provided with the
     *    distribution.
     *
     * 3. All advertising materials mentioning features or use of this
     *    software must display the following acknowledgment:
     *    "This product includes software developed by the OpenSSL Project
     *    for use in the OpenSSL Toolkit. (http://www.openssl.org/)"
     *
     * 4. The names "OpenSSL Toolkit" and "OpenSSL Project" must not be used to
     *    endorse or promote products derived from this software without
     *    prior written permission. For written permission, please contact
     *    openssl-core@openssl.org.
     *
     * 5. Products derived from this software may not be called "OpenSSL"
     *    nor may "OpenSSL" appear in their names without prior written
     *    permission of the OpenSSL Project.
     *
     * 6. Redistributions of any form whatsoever must retain the following
     *    acknowledgment:
     *    "This product includes software developed by the OpenSSL Project
     *    for use in the OpenSSL Toolkit (http://www.openssl.org/)"
     *
     * THIS SOFTWARE IS PROVIDED BY THE OpenSSL PROJECT ``AS IS'' AND ANY
     * EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
     * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
     * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE OpenSSL PROJECT OR
     * ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
     * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
     * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
     * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
     * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
     * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
     * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
     * OF THE POSSIBILITY OF SUCH DAMAGE.
     * ====================================================================
     *
     * This product includes cryptographic software written by Eric Young
     * (eay@cryptsoft.com).  This product includes software written by Tim
     * Hudson (tjh@cryptsoft.com).
     *
     */
    
     Original SSLeay License
     -----------------------
    
    /* Copyright (C) 1995-1998 Eric Young (eay@cryptsoft.com)
     * All rights reserved.
     *
     * This package is an SSL implementation written
     * by Eric Young (eay@cryptsoft.com).
     * The implementation was written so as to conform with Netscapes SSL.
     *
     * This library is free for commercial and non-commercial use as long as
     * the following conditions are aheared to.  The following conditions
     * apply to all code found in this distribution, be it the RC4, RSA,
     * lhash, DES, etc., code; not just the SSL code.  The SSL documentation
     * included with this distribution is covered by the same copyright terms
     * except that the holder is Tim Hudson (tjh@cryptsoft.com).
     *
     * Copyright remains Eric Young's, and as such any Copyright notices in
     * the code are not to be removed.
     * If this package is used in a product, Eric Young should be given attribution
     * as the author of the parts of the library used.
     * This can be in the form of a textual message at program startup or
     * in documentation (online or textual) provided with the package.
     *
     * Redistribution and use in source and binary forms, with or without
     * modification, are permitted provided that the following conditions
     * are met:
     * 1. Redistributions of source code must retain the copyright
     *    notice, this list of conditions and the following disclaimer.
     * 2. Redistributions in binary form must reproduce the above copyright
     *    notice, this list of conditions and the following disclaimer in the
     *    documentation and/or other materials provided with the distribution.
     * 3. All advertising materials mentioning features or use of this software
     *    must display the following acknowledgement:
     *    "This product includes cryptographic software written by
     *     Eric Young (eay@cryptsoft.com)"
     *    The word 'cryptographic' can be left out if the rouines from the library
     *    being used are not cryptographic related :-).
     * 4. If you include any Windows specific code (or a derivative thereof) from
     *    the apps directory (application code) you must include an acknowledgement:
     *    "This product includes software written by Tim Hudson (tjh@cryptsoft.com)"
     *
     * THIS SOFTWARE IS PROVIDED BY ERIC YOUNG ``AS IS'' AND
     * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
     * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
     * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
     * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
     * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
     * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
     * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
     * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
     * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
     * SUCH DAMAGE.
     *
     * The licence and distribution terms for any publically available version or
     * derivative of this code cannot be changed.  i.e. this code cannot simply be
     * copied and put under another distribution licence
     * [including the GNU Public Licence.]
     */



SDL2_image 2.0.4
~~~~~~~~~~~~~~~~

Copyright (C) 1997-2018 Sam Lantinga <slouken@libsdl.org>

https://www.libsdl.org/projects/SDL_image/



.. code-block:: none

    /*
      SDL_image:  An example image loading library for use with SDL
      Copyright (C) 1997-2018 Sam Lantinga <slouken@libsdl.org>
    
      This software is provided 'as-is', without any express or implied
      warranty.  In no event will the authors be held liable for any damages
      arising from the use of this software.
    
      Permission is granted to anyone to use this software for any purpose,
      including commercial applications, and to alter it and redistribute it
      freely, subject to the following restrictions:
    
      1. The origin of this software must not be misrepresented; you must not
         claim that you wrote the original software. If you use this software
         in a product, an acknowledgment in the product documentation would be
         appreciated but is not required.
      2. Altered source versions must be plainly marked as such, and must not be
         misrepresented as being the original software.
      3. This notice may not be removed or altered from any source distribution.
    */



SDL2_mixer 2.0.4
~~~~~~~~~~~~~~~~

Copyright (C) 1997-2018 Sam Lantinga <slouken@libsdl.org>

https://www.libsdl.org/projects/SDL_mixer/



.. code-block:: none

    /*
      SDL_mixer:  An audio mixer library based on the SDL library
      Copyright (C) 1997-2018 Sam Lantinga <slouken@libsdl.org>
    
      This software is provided 'as-is', without any express or implied
      warranty.  In no event will the authors be held liable for any damages
      arising from the use of this software.
    
      Permission is granted to anyone to use this software for any purpose,
      including commercial applications, and to alter it and redistribute it
      freely, subject to the following restrictions:
    
      1. The origin of this software must not be misrepresented; you must not
         claim that you wrote the original software. If you use this software
         in a product, an acknowledgment in the product documentation would be
         appreciated but is not required.
      2. Altered source versions must be plainly marked as such, and must not be
         misrepresented as being the original software.
      3. This notice may not be removed or altered from any source distribution.
    */



SDL2_ttf 2.0.14
~~~~~~~~~~~~~~~

Copyright (C) 1997-2016 Sam Lantinga <slouken@libsdl.org>

https://www.libsdl.org/projects/SDL_ttf/



.. code-block:: none

    /*
      SDL_ttf:  A companion library to SDL for working with TrueType (tm) fonts
      Copyright (C) 1997-2016 Sam Lantinga <slouken@libsdl.org>
    
      This software is provided 'as-is', without any express or implied
      warranty.  In no event will the authors be held liable for any damages
      arising from the use of this software.
    
      Permission is granted to anyone to use this software for any purpose,
      including commercial applications, and to alter it and redistribute it
      freely, subject to the following restrictions:
    
      1. The origin of this software must not be misrepresented; you must not
         claim that you wrote the original software. If you use this software
         in a product, an acknowledgment in the product documentation would be
         appreciated but is not required.
      2. Altered source versions must be plainly marked as such, and must not be
         misrepresented as being the original software.
      3. This notice may not be removed or altered from any source distribution.
    */



SDL2 2.0.9
~~~~~~~~~~

Copyright (C) 1997-2018 Sam Lantinga <slouken@libsdl.org>

https://www.libsdl.org/index.php



.. code-block:: none

    
    Simple DirectMedia Layer
    Copyright (C) 1997-2018 Sam Lantinga <slouken@libsdl.org>
    
    This software is provided 'as-is', without any express or implied
    warranty.  In no event will the authors be held liable for any damages
    arising from the use of this software.
    
    Permission is granted to anyone to use this software for any purpose,
    including commercial applications, and to alter it and redistribute it
    freely, subject to the following restrictions:
    
    1. The origin of this software must not be misrepresented; you must not
       claim that you wrote the original software. If you use this software
       in a product, an acknowledgment in the product documentation would be
       appreciated but is not required.
    2. Altered source versions must be plainly marked as such, and must not be
       misrepresented as being the original software.
    3. This notice may not be removed or altered from any source distribution.
    



sqlite3-amalgamation 3150100
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

https://www.sqlite.org/index.html



.. code-block:: none

    copyright note taken from sqlite.h of sqlite-amalgamation 3150100:
    
    2001 September 15
    
    The author disclaims copyright to this source code.  In place of
    a legal notice, here is a blessing:
    
       May you do good and not evil.
       May you find forgiveness for yourself and forgive others.
       May you share freely, never taking more than you give.


setuptools 40.9.0
~~~~~~~~~~~~~~~~~

Copyright (C) 2016 Jason R Coombs <jaraco@jaraco.com>

https://github.com/pypa/setuptools



.. code-block:: none

    
    Copyright (C) 2016 Jason R Coombs <jaraco@jaraco.com>
    
    Permission is hereby granted, free of charge, to any person obtaining a copy of
    this software and associated documentation files (the "Software"), to deal in
    the Software without restriction, including without limitation the rights to
    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
    of the Software, and to permit persons to whom the Software is furnished to do
    so, subject to the following conditions:
    
    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.
    
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.


six 1.10.0
~~~~~~~~~~

Copyright (c) 2010-2015 Benjamin Peterson

http://pypi.python.org/pypi/six/



.. code-block:: none

    
    Copyright (c) 2010-2015 Benjamin Peterson
    
    Permission is hereby granted, free of charge, to any person obtaining a copy of
    this software and associated documentation files (the "Software"), to deal in
    the Software without restriction, including without limitation the rights to
    use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
    the Software, and to permit persons to whom the Software is furnished to do so,
    subject to the following conditions:
    
    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.
    
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
    FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
    COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
    IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
    CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


pyjnius 1.2.1.dev0
~~~~~~~~~~~~~~~~~~

Copyright (c) 2010-2017 Kivy Team and other contributors

https://github.com/kivy/pyjnius



.. code-block:: none

    Copyright (c) 2010-2017 Kivy Team and other contributors
    
    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:
    
    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.
    
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.


python-for-android 2019.10.06
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Copyright (c) 2010-2017 Kivy Team and other contributors

https://python-for-android.readthedocs.io/en/latest/



.. code-block:: none

    Copyright (c) 2010-2017 Kivy Team and other contributors
    
    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:
    
    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.
    
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.


hidapi
~~~~~~

Copyright (c) 2010, Alan Ott, Signal 11 Software, All rights reserved.

part of SDL2 2.0.9 source code



.. code-block:: none

    HIDAPI can be used under one of three licenses.
    
    1. The GNU General Public License, version 3.0, in LICENSE-gpl3.txt
    2. A BSD-Style License, in LICENSE-bsd.txt.
    3. The more liberal original HIDAPI license. LICENSE-orig.txt
    
    The license chosen is at the discretion of the user of HIDAPI. For example:
    1. An author of GPL software would likely use HIDAPI under the terms of the
    GPL.
    
    2. An author of commercial closed-source software would likely use HIDAPI
    under the terms of the BSD-style license or the original HIDAPI license.
    
    
    Below the content of the LICENSE-bsd.txt file are given:
    
    Copyright (c) 2010, Alan Ott, Signal 11 Software
    All rights reserved.
    
    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:
    
        * Redistributions of source code must retain the above copyright notice,
          this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in the
          documentation and/or other materials provided with the distribution.
        * Neither the name of Signal 11 Software nor the names of its
          contributors may be used to endorse or promote products derived from
          this software without specific prior written permission.
    
    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
    POSSIBILITY OF SUCH DAMAGE.


docutils
~~~~~~~~

http://docutils.sourceforge.net/



.. code-block:: none

    ==================
     Copying Docutils
    ==================
    
    :Author: David Goodger
    :Contact: goodger@python.org
    :Date: $Date: 2020-02-06 12:47:25 +0000 (Thu, 06 Feb 2020) $
    :Web site: http://docutils.sourceforge.net/
    :Copyright: This document has been placed in the public domain.
    
    Most of the files included in this project have been placed in the
    public domain, and therefore have no license requirements and no
    restrictions on copying or usage; see the `Public Domain Dedication`_
    below.  There are a few exceptions_, listed below.
    Files in the Sandbox_ are not distributed with Docutils releases and
    may have different license terms.
    
    
    Public Domain Dedication
    ========================
    
    The persons who have associated their work with this project (the
    "Dedicator": David Goodger and the many contributors to the Docutils
    project) hereby dedicate the entire copyright, less the exceptions_
    listed below, in the work of authorship known as "Docutils" identified
    below (the "Work") to the public domain.
    
    The primary repository for the Work is the Internet World Wide Web
    site <http://docutils.sourceforge.net/>.  The Work consists of the
    files within the "docutils" module of the Docutils project Subversion
    repository (Internet host docutils.svn.sourceforge.net, filesystem path
    /svnroot/docutils), whose Internet web interface is located at
    <http://docutils.svn.sourceforge.net/viewvc/docutils/>.  Files dedicated to the
    public domain may be identified by the inclusion, near the beginning
    of each file, of a declaration of the form::
    
        Copyright: This document/module/DTD/stylesheet/file/etc. has been
                   placed in the public domain.
    
    Dedicator makes this dedication for the benefit of the public at large
    and to the detriment of Dedicator's heirs and successors.  Dedicator
    intends this dedication to be an overt act of relinquishment in
    perpetuity of all present and future rights under copyright law,
    whether vested or contingent, in the Work.  Dedicator understands that
    such relinquishment of all rights includes the relinquishment of all
    rights to enforce (by lawsuit or otherwise) those copyrights in the
    Work.
    
    Dedicator recognizes that, once placed in the public domain, the Work
    may be freely reproduced, distributed, transmitted, used, modified,
    built upon, or otherwise exploited by anyone for any purpose,
    commercial or non-commercial, and in any way, including by methods
    that have not yet been invented or conceived.
    
    (This dedication is derived from the text of the `Creative Commons
    Public Domain Dedication`. [#]_)
    
    .. [#] Creative Commons has `retired this legal tool`__ and does not
       recommend that it be applied to works: This tool is based on United
       States law and may not be applicable outside the US. For dedicating new
       works to the public domain, Creative Commons recommend the replacement
       Public Domain Dedication CC0_ (CC zero, "No Rights Reserved"). So does
       the Free Software Foundation in its license-list_.
    
       __  http://creativecommons.org/retiredlicenses
       .. _CC0: http://creativecommons.org/about/cc0
    
    Exceptions
    ==========
    
    The exceptions to the `Public Domain Dedication`_ above are:
    
    * docutils/writers/s5_html/themes/default/iepngfix.htc:
    
          IE5.5+ PNG Alpha Fix v1.0 by Angus Turnbull
          <http://www.twinhelix.com>.  Free usage permitted as long as
          this notice remains intact.
    
    * docutils/utils/math/__init__.py,
      docutils/utils/math/latex2mathml.py,
      docutils/writers/xetex/__init__.py,
      docutils/utils/error_reporting.py,
      docutils/test/transforms/test_smartquotes.py:
    
      Copyright © Günter Milde.
      Released under the terms of the `2-Clause BSD license`_
      (`local copy <licenses/BSD-2-Clause.txt>`__).
    
    * docutils/utils/smartquotes.py
    
      Copyright © 2011 Günter Milde,
      based on `SmartyPants`_ © 2003 John Gruber
      (released under a 3-Clause BSD license included in the file)
      and smartypants.py © 2004, 2007 Chad Miller.
      Released under the terms of the `2-Clause BSD license`_
      (`local copy <licenses/BSD-2-Clause.txt>`__).
    
      .. _SmartyPants: http://daringfireball.net/projects/smartypants/
    
    * docutils/utils/math/math2html.py,
      docutils/writers/html4css1/math.css
    
      Copyright © Alex Fernández
      These files are part of eLyXer_, released under the `GNU
      General Public License`_ version 3 or later. The author relicensed
      them for Docutils under the terms of the `2-Clause BSD license`_
      (`local copy <licenses/BSD-2-Clause.txt>`__).
    
      .. _eLyXer: http://www.nongnu.org/elyxer/
    
    * docutils/utils/roman.py, copyright by Mark Pilgrim, released under the
      `Python 2.1.1 license`_ (`local copy`__).
    
      __ licenses/python-2-1-1.txt
    
    * tools/editors/emacs/rst.el, copyright by Free Software Foundation,
      Inc., released under the `GNU General Public License`_ version 3 or
      later (`local copy`__).
    
      __ licenses/gpl-3-0.txt
    
    The `2-Clause BSD license`_ and the Python licenses are OSI-approved_
    and GPL-compatible_.
    
    Plaintext versions of all the linked-to licenses are provided in the
    licenses_ directory.
    
    .. _sandbox: http://docutils.sourceforge.net/sandbox/README.html
    .. _licenses: licenses/
    .. _Python 2.1.1 license: http://www.python.org/2.1.1/license.html
    .. _GNU General Public License: http://www.gnu.org/copyleft/gpl.html
    .. _2-Clause BSD license: http://www.spdx.org/licenses/BSD-2-Clause
    .. _OSI-approved: http://opensource.org/licenses/
    .. _license-list:
    .. _GPL-compatible: http://www.gnu.org/licenses/license-list.html



pygments
~~~~~~~~

Copyright (c) 2006-2017 by the respective authors (see AUTHORS file). All rights reserved.

https://pygments.org/



.. code-block:: none

    Copyright (c) 2006-2017 by the respective authors (see AUTHORS file).
    All rights reserved.
    
    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are
    met:
    
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    
    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
    A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
    OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
    LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.



glew
~~~~

Copyright (C) 2008-2016, Nigel Stewart <nigels[]users sourceforge net>
Copyright (C) 2002-2008, Milan Ikits <milan ikits[]ieee org>
Copyright (C) 2002-2008, Marcelo E. Magallon <mmagallo[]debian org>
Copyright (C) 2002, Lev Povalahev
All rights reserved.

http://glew.sourceforge.net/



.. code-block:: none

    The OpenGL Extension Wrangler Library
    Copyright (C) 2008-2016, Nigel Stewart <nigels[]users sourceforge net>
    Copyright (C) 2002-2008, Milan Ikits <milan ikits[]ieee org>
    Copyright (C) 2002-2008, Marcelo E. Magallon <mmagallo[]debian org>
    Copyright (C) 2002, Lev Povalahev
    All rights reserved.
    
    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:
    
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * The name of the author may be used to endorse or promote products
      derived from this software without specific prior written permission.
    
    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
    LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
    CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
    SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
    INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
    CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
    THE POSSIBILITY OF SUCH DAMAGE.


simple-plotter 0.3.2
~~~~~~~~~~~~~~~~~~~~

Copyright (C) 2019-2020  Thies Hecker

https://simple-plotter.readthedocs.io/en/latest/



.. code-block:: none

    simple-plotter - simple plot code generator and GUI front-end
    Copyright (C) 2019-2020  Thies Hecker
    
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
    
    
    License for color map used in advance_graph module
    ==================================================
    
    The color table in standard_colors method is taken from matplotlib v3.1.3:
    https://github.com/matplotlib/matplotlib/blob/v3.1.3/lib/matplotlib/_cm.py
    
    Copyright (c) 2012-2020 Matplotlib Development Team; All Rights Reserved
    
    See the matplotlib license agreement below.
    
    License agreement for matplotlib versions 1.3.0 and later
    =========================================================
    
    1. This LICENSE AGREEMENT is between the Matplotlib Development Team
    ("MDT"), and the Individual or Organization ("Licensee") accessing and
    otherwise using matplotlib software in source or binary form and its
    associated documentation.
    
    2. Subject to the terms and conditions of this License Agreement, MDT
    hereby grants Licensee a nonexclusive, royalty-free, world-wide license
    to reproduce, analyze, test, perform and/or display publicly, prepare
    derivative works, distribute, and otherwise use matplotlib
    alone or in any derivative version, provided, however, that MDT's
    License Agreement and MDT's notice of copyright, i.e., "Copyright (c)
    2012- Matplotlib Development Team; All Rights Reserved" are retained in
    matplotlib  alone or in any derivative version prepared by
    Licensee.
    
    3. In the event Licensee prepares a derivative work that is based on or
    incorporates matplotlib or any part thereof, and wants to
    make the derivative work available to others as provided herein, then
    Licensee hereby agrees to include in any such work a brief summary of
    the changes made to matplotlib .
    
    4. MDT is making matplotlib available to Licensee on an "AS
    IS" basis.  MDT MAKES NO REPRESENTATIONS OR WARRANTIES, EXPRESS OR
    IMPLIED.  BY WAY OF EXAMPLE, BUT NOT LIMITATION, MDT MAKES NO AND
    DISCLAIMS ANY REPRESENTATION OR WARRANTY OF MERCHANTABILITY OR FITNESS
    FOR ANY PARTICULAR PURPOSE OR THAT THE USE OF MATPLOTLIB
    WILL NOT INFRINGE ANY THIRD PARTY RIGHTS.
    
    5. MDT SHALL NOT BE LIABLE TO LICENSEE OR ANY OTHER USERS OF MATPLOTLIB
     FOR ANY INCIDENTAL, SPECIAL, OR CONSEQUENTIAL DAMAGES OR
    LOSS AS A RESULT OF MODIFYING, DISTRIBUTING, OR OTHERWISE USING
    MATPLOTLIB , OR ANY DERIVATIVE THEREOF, EVEN IF ADVISED OF
    THE POSSIBILITY THEREOF.
    
    6. This License Agreement will automatically terminate upon a material
    breach of its terms and conditions.
    
    7. Nothing in this License Agreement shall be deemed to create any
    relationship of agency, partnership, or joint venture between MDT and
    Licensee.  This License Agreement does not grant permission to use MDT
    trademarks or trade name in a trademark sense to endorse or promote
    products or services of Licensee, or any third party.
    
    8. By copying, installing or otherwise using matplotlib ,
    Licensee agrees to be bound by the terms and conditions of this License
    Agreement.



