Roadmap and Issue-tracker
=========================

For bug-reports and feature request please use the Issue tracker on the project's gitlab-page:

https://gitlab.com/thecker/simple-plotter/-/issues

The development roadmap can be found on gitlab as well:

see https://gitlab.com/thecker/simple-plotter/-/milestones


